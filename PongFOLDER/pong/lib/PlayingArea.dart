import 'package:flutter/material.dart';
import 'package:firebase_database/firebase_database.dart';
import 'package:flutter/services.dart';
import 'package:spritewidget/spritewidget.dart';
import 'dart:ui';
import 'main.dart';

NodeWithSize rootNode;
Size paddleSize = new Size(200, 50);
SpritePaddle paddleA;
SpritePaddle paddleB;
SpriteBall ball;
class PlayingArea extends StatefulWidget
{
  State createState()=> new PlayingAreaState();
}

class PlayingAreaState extends State<PlayingArea>
{
  void initState()
  {
     database = new FirebaseDatabase(app: firebaseApp);
     SystemChrome.setEnabledSystemUIOverlays([]);

     rootNode = new NodeWithSize(const Size(1024.0, 1024.0));  
     paddleA = SpritePaddle(name:"Paddle A", color:Colors.orange,size:paddleSize,positionDx:400,positionDy: 900 );
     paddleB = SpritePaddle(name:"Paddle B" ,color:Colors.blue,size:paddleSize,positionDx:400,positionDy: 100 );
     ball = SpriteBall(name: "Ball", color: Colors.purple, radius:15.0,positionDx:500,positionDy:500);

     rootNode.addChild(paddleA);
     rootNode.addChild(paddleB);
     rootNode.addChild(ball);
  }
  @override
  Widget build(BuildContext context)
  {
    return Scaffold
    (
      body: new SpriteWidget(rootNode) ,
    );
  }
}

class Paddle extends StatefulWidget //! Outdated implementation for paddle using stateful widget
{
  Offset offset;
  Color color; 

  Paddle({this.offset,this.color});
  @override
  State createState()=> new PaddleState(offset: offset,color:color);
}

class PaddleState extends State<Paddle>//! Outdated implementation for paddle using stateful widget
{
   Offset offset;
   Color color;
   double width , height;

   PaddleState({this.offset,this.color});
   @override
   void initState()
   {
     if (offset == null)
     {
       print(offset.toString());
       offset = Offset(0.0,0.0);
     }
     if(color==null)
     {
       color = Colors.black;
     }
     if(width == null || height == null)
     {
       width = 150.0; 
       height = 25.0;
     }
   }
   @override
   Widget build(BuildContext context)
   {
     return new Positioned
      (
        top:  offset.dy,
        left: offset.dx,
        child: new Draggable
        (
          maxSimultaneousDrags: 1,
          axis: Axis.horizontal,
          child: new Container
          (
            color: color,
            height: height,
            width: width,
          ),
          feedback: new Container
          (
            color: color,
            height: height,
            width: width,
          ), 
          childWhenDragging: new Container
          (
            height:height,
            width: width,

          ),
          
          onDraggableCanceled:(v,o)
          {
            setState(() 
              {
                if(o.dx < 220.0 && o.dx > 0.0)
                {
                  offset= o;  
                }  
                else
                {
                  if(o.dx < 10)
                  {
                    offset = Offset(0.0,o.dy);
                  }
                  else
                  {
                     offset = Offset(220.0,o.dy);
                  }
                }            
              }
            );
          }
        ) ,
      );
   }
 } 

class SpritePaddle extends NodeWithSize
{
  Rect rect;
  Color color;
  Size size ; 
  String name; 
  double positionDx,positionDy,speedFactor = 5;
  Paint draw= new Paint();

  SpritePaddle({ @required this.name,this.size , this.color,this.positionDx,this.positionDy}) : super(size) 
  {
    userInteractionEnabled = true;
    handleMultiplePointers =true;
  }

  @override
  void paint(Canvas canvas)
  {
    canvas.save();

    position = new Offset(positionDx,positionDy);
    rect = new Rect.fromLTWH(0.0, 0.0, size.width,size.height);
    draw.color = color;
    
    canvas.drawRect(rect, draw);

    canvas.restore();
    
  }
  @override
  void update(double dt)
  { 
    positionDx = positionDx;
  }
 
  @override
  bool handleEvent(SpriteBoxEvent event) 
  {
    switch(event.type)
    {
      case PointerDownEvent:// when key input is produced 
       
      break;

      case PointerMoveEvent: //triggers when touch is recieved from within the box/sprite 
        
        if(convertPointToNodeSpace(event.boxPosition).dx > 0) // checking which direction the key input is going 
        {
          // going right
          positionDx +=speedFactor;
          if(positionDx >570.0) // prevent from getting out of bounds
          {
            positionDx = 570.0;
          }
        }
        else 
        {
          //going left 
          positionDx -=speedFactor;
          if(positionDx < 200.0) // allow for "smoother" transition to the end (still sucks hahahah fuck me)
          {
            positionDx = 200.0;
          }
        }
      break;
    }

    return super.handleEvent(event);
  }
}

class SpriteBall extends Node
{
  Color color;
  Size size ; 
  String name; 
  Offset position;
  double positionDx , positionDy,radius ,speedFactorDy= 5.0 ,speedFactorDx = 0.0 ;
  bool directionY = true , directionX =true; //true is moving up , false is down
  Paint draw= new Paint();

  SpriteBall({@required this.name,this.radius, this.color,this.positionDx,this.positionDy})
  {
    userInteractionEnabled =false;
    handleMultiplePointers =false;
  }

  @override
  void paint(Canvas canvas)
  {
    canvas.save();
    position = new Offset(positionDx,positionDy);
    draw.color = color;
    canvas.drawCircle(position, radius, draw);
    canvas.restore();
    
  }
  @override
  void update(double dt)
  { 
    // ? use the below code for debugging 
    //print("Pure: "+paddleA.position.dx.toString()+ " Min: "+(paddleA.position.dx-100).toString()+" Max: "+(paddleA.position.dx+100).toString() +" Ball Coordinates: "+position.toString());
  
    if(directionY == true) // moving up 
    {
      positionDy -= speedFactorDy; 
      positionDx -= speedFactorDx;
      if(positionDy == paddleB.position.dy+paddleSize.height+10 ) // At paddle Y position checking of X collision
      {
        double hitPositionDx= paddleB.position.dx+200 - positionDx;
        if(positionDx >= paddleB.position.dx && positionDx <= paddleB.position.dx+200 ) // Collison detection for paddle
        {
          // place trajectory code here 
          double hitPositionDx= paddleB.position.dx+200 - positionDx;
          if(hitPositionDx>100)
          {
            double newSpeedDx = (hitPositionDx-100.0)/10;
            print(newSpeedDx);
            speedFactorDx = newSpeedDx;
          }
          else if(hitPositionDx<100)
          {
            double newSpeedDx = (hitPositionDx-100.0)/10;
            print(newSpeedDx);
            speedFactorDx = newSpeedDx;
          }
          directionY = false; 
        }
      }
      if(positionDx <= 220.0|| positionDx >= 770.0)
      {
        speedFactorDx = -speedFactorDx;
      }
      if(positionDy <= 10 ) // hitting the boundries of the playing area
      {
        speedFactorDy = 0;
        speedFactorDx =0;
        // place reset code here 
        // place counter for points code here 
      }
    }

    else // moving down
    {
      positionDy += speedFactorDy;
      positionDx += speedFactorDx;
       
      if(positionDy == paddleA.position.dy-10.0 ) // At paddle Y position checking of X collision
      {
        if(positionDx >= paddleA.position.dx && positionDx <= paddleA.position.dx+200 ) // Collison detection for paddle
        {
          // place trajectory code here 
          double hitPositionDx= paddleA.position.dx+200 - positionDx;
          if(hitPositionDx>100)
          {
            double newSpeedDx = (hitPositionDx-100.0)/10;
            print(newSpeedDx);
            speedFactorDx = newSpeedDx;
          }
          else if(hitPositionDx<100)
          {
            double newSpeedDx = (hitPositionDx-100.0)/10;
            print(newSpeedDx);
            speedFactorDx = newSpeedDx;
          }
          directionY = true; 
          
        }
      }
      if(positionDx <= 220.0|| positionDx >= 770.0)
      {
        speedFactorDx = -speedFactorDx;
      }
      if(positionDy == 1010 ) // hitting the boundries of the playing area
      {
        speedFactorDy = 0;
        speedFactorDx = 0 ;
        // place reset code here 
        // place counter for points code here 
      }
    }
  }
}