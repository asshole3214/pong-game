import 'package:flutter/material.dart';
import 'package:firebase_core/firebase_core.dart';
import 'package:firebase_database/firebase_database.dart';
import 'dart:async';
import 'PlayingArea.dart';

FirebaseApp firebaseApp;
FirebaseDatabase database;

void main() async
{
  firebaseApp = await FirebaseApp.configure(
    name: 'Pong',
    options: 
        const FirebaseOptions(
          googleAppID: '1:303965902397:android:b37e1d545b36a173',
          apiKey: 'AIzaSyBqP_PdT7VE0RWYUu4vTth3dTI7SWKNHBo',
          databaseURL:"https://pong-c0b66.firebaseio.com/",
        ),
  );
 
  runApp
  (
    new MaterialApp
    (
      debugShowCheckedModeBanner: false,
      home : new PlayingArea(),
    )
  );
}

